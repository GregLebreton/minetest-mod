-- chargement du mod avec appelation du node (ici block) --
minetest.register_node("mon_mod:special_bloc", {
    -- description visible dans l'inventaire -- 
    -- toutes les propriétés possibles https://minetest.gitlab.io/minetest/definition-tables/#node-definition --
    description = "mon special bloc",
    -- reférencenment de la ressource png --
    tiles = {"default_ice.png"},
    -- définition du matériau (avec quoi on le détruit et le son lors de la destruction) -> voir doc groups--
    groups = {oddly_breakable_by_hand=1},
})

-- le crafting de notre special bloc --
minetest.register_craft({
    -- sortie du craft nombre = nombre de blocs craftés
    output = "mon_mod:special_bloc",
    recipe = {
        {"default:snow", "default:snow", "default:snow"},
        {"default:snow", "bucket:bucket_water", "default:snow"},
        {"default:snow", "default:snow", "default:snow"}       
    }
})

-- ce qui va modifier les blocs / items etc... (ABM = Active Bloc Modifier) --
minetest.register_abm({
    -- optionnel --
    label = "mon_mod:special_bloc",
    -- nom des blocs sur lesquels la modification va s'appliquer --
    nodenames = {"mon_mod:special_bloc"},
    -- intervalle en secondes
    interval = 10,
    -- probabilité que cela se produise (proba = 1/chance) --
    chance = 1,
    -- action de l'ABM --
    action = function(pos, node)
        minetest.add_node(pos, {name = "default:water_source"})
    end,
})