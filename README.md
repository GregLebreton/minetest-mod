# MINETEST MOD

![minetest](docs/minetest.png)

## [COMPOSITION D'UN MOD]

Une fois le dossier "mon_mod" créer dans le dossier /usr/share/games/minetest/games/minetest_game/mods/ (linux), il faut créer trois fichiers:

- mod.conf
- init.lua
- screenshot.png

C'est le minimum si on ajoute aucune nouvelle ressource commme des textures par exemple auquel cas il nous faudrait créer un dossier "textures" pour acueillir nos fichiers au format png.

#### MOD.CONF

C'est le fichier de configuration du mod. il y est décrit le nom du mod, sa description, son auteur, ce que l'on peux qualifié de metadonnée.

```conf
# nom du mod dans le menu ingame
name = La glace fond!
# referencement des ressources du jeu par défaut
depends = default, bucket
# petite description apparaissant dans l
description = "un bloc de glace se transformant en source d'eau. Pour le crafter vous aurez besoin de 8 boules de neige et un sceau d'eau"
# tire du mod
title = la glace fond!
# auteur du mod
author = yannis
```

#### INIT.LUA

C'est le code du mod, c'est ici que l'on va envoyer des informations à l'API du jeux pour lui indiquer ce que va faire notre mod, les fonctionnalités qu'il va ajouter, ce que cela va changer dans le jeu original.

```lua
-- chargement du mod avec appelation du node (ici block) --
minetest.register_node("glace:la_glace_fond", {
    -- description visible dans l'inventaire -- 
    -- toutes les propriétés possibles https://minetest.gitlab.io/minetest/definition-tables/#node-definition --
    description = "La glace fond!",
    -- reférencenment de la ressource png --
    tiles = {"default_ice.png"},
    -- définition du matériau (avec quoi on le détruit et le son lors de la destruction) -> voir doc groups --
    groups = {oddly_breakable_by_hand=1},
})

-- le crafting de notre special bloc --
minetest.register_craft({
    -- sortie du craft nombre = nombre de blocs craftés --
    output = "glace:la_glace_fond",
    recipe = {
        {"default:snow", "default:snow", "default:snow"},
        {"default:snow", "bucket:bucket_water", "default:snow"},
        {"default:snow", "default:snow", "default:snow"}       
    }
})

-- ce qui va modifier les blocs / items etc... (ABM = Active Bloc Modifier) --
minetest.register_abm({
    -- optionnel --
    label = "glace:la_glace_fond",
    -- nom des blocs sur lesquels la modification va s'appliquer --
    nodenames = {"glace:la_glace_fond"},
    -- intervalle en secondes avant appel à la fonction action --
    interval = 10,
    -- probabilité que cela se produise (proba = 1/chance) --
    chance = 1,
    -- action de l'ABM --
    action = function(pos, node)
        minetest.add_node(pos, {name = "default:water_source"})
    end,
})
```

#### SCREENSHOT

c'est une image pour présenter le mod dans l'onglet "contenu" du menu du jeu lors du démarrage.

## [POSSIBILITÉS]

En consultant la rubrique ressources juste après, on peux voir les différents exemples de code permettant de créer un mod.
Cela va de la création d'un bloc intéragissant avec l'environnement, à un bloc modifiant les capacités du joueur ou tout autre chose qui vous inspire, il convient juste de comprendre le fonctionement de l'API pour lui indiquer comme vous voulez modifier le monde. 

## [CRÉATION DE TEXTURE]

Pour créer une texture, il suffit de créer une image au bon format, de la placer dans le dossier texture de son mod et de la renseigner dans le fichier init.lua au niveau de la propriété "tiles" avec la possibilité de définir l'apparence de chaque face de notre cube. 

> Si aucun paramètre n'est renseigné, le cube aurat les six faces identiques.

![herbe](docs/herbe.png) ![terre](docs/terre.png)

## [RESSOURCES]

- [Minetest modding book by Ruben Wardy](https://rubenwardy.com/minetest_modding_book/en/index.html)
- [La documentation faite par les créateurs de Minetest](https://minetest.gitlab.io/minetest/mods/)
    - [Définition d'un node](https://minetest.gitlab.io/minetest/definition-tables/#node-definition)
- [Wiki de Minetest](https://wiki.minetest.net/)

>le code du mod exemple est créé par Yannis Yahiaoui

